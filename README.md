# Example C/C++ pipeline for GitLab

Here is simple parametrized GitLab Pipeline to build both `x86_64` 64-bit  and `x86` 32-bit binaries
using public Docker images

# Setup

Tested on:
- OS: `openSUSE Leap 15.1` on `x86_64` platform
- `GitLab CE 13.0.6` rpm installation - tested `gitlab-ce-13.0.6-ce.0.sles15.x86_64.rpm`
- `gitlab-runner` registered to GitLab and configured with `docker executor` - must
  have tag `docker` to match this pipeline tag.

> NOTE: GitLab and `gitlab-runner` setup is out of scope of this readme.

You need to install following packages on `gitlab-runner` on openSUSE:

```bash
sudo zypper in git-core docker
```
Ensure that docker is running before commiting this project.

Then create new project in your GitLab and push this project to your GitLab. It should run
Pipeline in this project...


# Resources

* Template is based on https://stackoverflow.com/a/41199878

